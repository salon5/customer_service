package db

// func ConnectToDB(cfg config.Config) (*sqlx.DB, error) {
// 	psqlString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
// 	cfg.PostgresHost,
// 	cfg.PostgresPort,
// 	cfg.PostgresUser,
// 	cfg.PostgresPassword,
// 	cfg.PostgresDatabase)

// 	connDb, err := sqlx.Connect("postgres", psqlString)
// 	if err != nil {
// 		return nil, err	
// 	}

// 	return connDb, nil

// }	

// func ConnectToDBForSuite(cfg config.Config) (*sqlx.DB, func()) {
// 	psqlString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslomode=disable",
// 	cfg.PostgresHost,
// 	cfg.PostgresPort,
// 	cfg.PostgresUser,
// 	cfg.PostgresPassword,
// 	cfg.PostgresDatabase)

// 	connDb, err := sqlx.Connect("postgres", psqlString)
// 	if err != nil {
// 		return nil, func() {}
// 	}
// 	cleaunUp := func ()  {
// 		connDb.Close()
// 	}

// 	return connDb, cleaunUp
	
// }


import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4/pgxpool"
)

const (
	_defaultMaxPoolSize  = 1
	_defaultConnAttempts = 10
	_defaultConnTimeout  = time.Second
)

type Postgres struct {
	maxPoolSize  int
	connAttempts int
	connTimeout  time.Duration
	Builder      squirrel.StatementBuilderType
	Pool         *pgxpool.Pool
}

func New(url string, opts ...Option) (*Postgres, error) {
	pg := &Postgres{
		maxPoolSize: _defaultMaxPoolSize,
		connAttempts: _defaultConnAttempts,
		connTimeout: _defaultConnTimeout,
	}

	for _, opt := range opts {
		opt(pg)
	}

	pg.Builder = squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)
	
	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		return nil, fmt.Errorf("postgres - NewPostgres - pgxpool.ParseConfig: %w", err)
	} 

	poolConfig.MaxConns = int32(pg.maxPoolSize)
	
	for pg.connAttempts > 0 {
		pg.Pool, err = pgxpool.ConnectConfig(context.Background(), poolConfig)
		if err == nil {
			break
		}
		log.Printf("Postgres is trying to connect, attempts left: %d", pg.connAttempts)

		time.Sleep(pg.connTimeout)

		pg.connAttempts--
	}
	if err != nil {
		return nil, fmt.Errorf("postgres - NewPostgres - connAttempts == 0: %w", err)
	}
	return pg, nil

}

func (p *Postgres) Close() {
	if p.Pool != nil {
		p.Pool.Close()
	}
}