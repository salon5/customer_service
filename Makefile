run:
	go run cmd/app/main.go

proto-gen:
	./internal/script/gen-proto.sh

update_submodule:
	git submodule update --remote --merge

create_proto_submodule:
	git submodule add git@gitlab.com:adds-uz/protos.git

migrate_up:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/customer_evron up

migrate_down:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/customer_evron down

migrate_force:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/customer_evron force 5
	
# migrate create -ext sql -dir migrations -seq create_customers_table
 
  