CREATE TABLE IF NOT EXISTS address(
    id SERIAL PRIMARY KEY,
    customer_id INT NOT NULL REFERENCES customers(id),
    district TEXT NOT NULL,
    street TEXT NOT NULL,
    homenumber TEXT NOT NULL
);