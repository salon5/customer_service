package postgres

import (
	"context"
	"fmt"
	"time"

	cc "gitlab.com/salon5/customer_service/internal/genproto/customer"
)


func (c *CustomerRepo) CreateCustomer(req *cc.CreateCustomerReq) (*cc.CreateCustomerResp, error) {
	responce := &cc.CreateCustomerResp{}
	create_time := time.Time{}
	updated_time := time.Time{}
	
	err := c.Db.Pool.QueryRow(context.Background(), `
	INSERT INTO 
		customers(fullname, email, phonenumber, salon_id, auto_id) 
		VALUES($1, $2, $3, $4, $5)
		returning
		id, 
		fullname,
		email,
		phonenumber,
		salon_id,
		auto_id,
		created_at,
		updated_at`,
		req.Fullname,
		req.Email,
		req.Phonenumber,
		req.SalonId,
		req.AutoId,
	).Scan(
		&responce.Id,
		&responce.Fullname,
		&responce.Email,
		&responce.Phonenumber,
		&responce.SalonId,
		&responce.AutoId,
		&create_time,
		&updated_time,
	)
	if err != nil {
		fmt.Println(err)
		return &cc.CreateCustomerResp{}, err
	}
	
	for _, insert := range req.Addresses {
		insert.CustomerId = responce.Id
		_ ,err := c.CreateAddress(insert)
		if err != nil {
			fmt.Println(`Error inserting Address in Customer`,err)
			return &cc.CreateCustomerResp{}, err
		}
		responce.Addresses = append(responce.Addresses, insert)
	}
	responce.CreatedAt = create_time.String()
	responce.UpdatedAt = updated_time.String()

	return responce, nil
}

func (c *CustomerRepo) GetCustomers(req *cc.CustomerID) (*cc.GetCustomerResp, error) {
	responce := &cc.GetCustomerResp{}
	err := c.Db.Pool.QueryRow(context.Background(), `
	SELECT id, fullname, email, phonenumber, salon_id, auto_id FROM customers WHERE id=$1 and deleted_at IS NULL
	`,req.CustomerId).Scan(
		&responce.Id,
		&responce.Fullname,
		&responce.Email,
		&responce.Phonenumber,
		&responce.SalonId,
		&responce.AutoId,
	)
	if err != nil {
		return &cc.GetCustomerResp{}, err
	}
	
	responce.Addresses, err = c.GetAddress(req)
	if err != nil {
		return &cc.GetCustomerResp{}, err
	}

return responce, nil	
}