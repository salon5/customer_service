package grpcclient

import (
	"fmt"

	"gitlab.com/salon5/customer_service/config"
	aa "gitlab.com/salon5/customer_service/internal/genproto/autoinfo"
	ss "gitlab.com/salon5/customer_service/internal/genproto/salon"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Clients interface {
	Salon() ss.SalonServiceClient
	AutoInfo() aa.AutoInfoServiceClient
}

type ServiceManager struct {
	Config config.Config
	salonService ss.SalonServiceClient
	autoInfoService aa.AutoInfoServiceClient
}

func New(cfg config.Config) (*ServiceManager, error) {
	connSalon, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.SalonServiceHost, cfg.SalonServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &ServiceManager{}, err
	}

	connAutoInfo, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.AutoInfoServiceHost, cfg.AutoInfoServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &ServiceManager{}, err
	}

	return &ServiceManager{
		Config: cfg,
		salonService: ss.NewSalonServiceClient(connSalon),
		autoInfoService: aa.NewAutoInfoServiceClient(connAutoInfo),
	},nil

}

func (s *ServiceManager) Salon() ss.SalonServiceClient {
	return s.salonService
}

func (s *ServiceManager) AutoInfo() aa.AutoInfoServiceClient {
	return s.autoInfoService
}