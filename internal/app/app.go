package app

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/salon5/customer_service/config"
	"gitlab.com/salon5/customer_service/internal/controller/service"
	grpcclient "gitlab.com/salon5/customer_service/internal/controller/service/grpcClient"
	"gitlab.com/salon5/customer_service/internal/genproto/customer"
	"gitlab.com/salon5/customer_service/pkg/db"
	"gitlab.com/salon5/customer_service/pkg/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)


func Run(cfg *config.Config) {
	l := logger.New(cfg.LogLevel)

	pgxUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
	cfg.PostgresUser,
	cfg.PostgresPassword,
	cfg.PostgresHost,
	cfg.PostgresPort,
	cfg.PostgresDatabase)

	pg, err := db.New(pgxUrl, db.MaxPoolSize(cfg.PGXPOOLMAX))
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - postgres.New: %w", err))
	}
defer pg.Close()

	clients, err := grpcclient.New(*cfg)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcclient.New: %w", err))
	}

	customerService := service.NewCustomerService(l, clients, pg)

	lis, err := net.Listen("tcp", ":" + cfg.CustomerServicePort)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcClient.New: %w", err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	customer.RegisterCustomerServiceServer(c, customerService)

	l.Info("Server is running on" + "port" + ":" + cfg.CustomerServicePort)

	if err := c.Serve(lis); err != nil {
		log.Fatal("Error while listening: ", err)
	}

} 