package postgres

import (
	"context"
	"fmt"

	cc "gitlab.com/salon5/customer_service/internal/genproto/customer"
)

func (c *CustomerRepo) CreateAddress(req *cc.Address) (*cc.Address, error) {
	res := &cc.Address{}
	fmt.Println(`Enter`)
	err := c.Db.Pool.QueryRow(context.Background(), `
	INSERT INTO address(customer_id, district, street, homenumber) 
	VALUES($1, $2, $3, $4) returning id, customer_id, district, street, homenumber`,
	req.CustomerId, 
	req.District, 
	req.Street, 
	req.Homenumber).Scan(
		&res.Id,
		&res.CustomerId,
		&res.District,
		&res.Street,
		&res.Homenumber,
	)
	if err != nil {
		return &cc.Address{}, err
	}
	fmt.Println("Exit")

return res, nil
}

func (c *CustomerRepo) GetAddress(req *cc.CustomerID) ([]*cc.Address, error) {
	responce := []*cc.Address{}
	rows, err := c.Db.Pool.Query(context.Background(), `SELECT id, customer_id, district, street, homenumber FROM address WHERE customer_id=$1`,req.CustomerId)
	if err != nil {
		return nil, err 
	}
	defer rows.Close()


	for rows.Next(){
		address := &cc.Address{}
		err := rows.Scan(
			&address.Id,
			&address.CustomerId,
			&address.District,
			&address.Street,
			&address.Homenumber,
		)
		if err != nil {
			return nil, err
		}
		responce = append(responce, address)
	}

	return responce, nil
}