package service

import (
	grpcclient "gitlab.com/salon5/customer_service/internal/controller/service/grpcClient"
	"gitlab.com/salon5/customer_service/internal/controller/storage"
	"gitlab.com/salon5/customer_service/pkg/db"
	"gitlab.com/salon5/customer_service/pkg/logger"
)

type CustomerService struct {
	Logger *logger.Logger
	Clients grpcclient.Clients
	Storage storage.IStorage
}

func NewCustomerService(l *logger.Logger, client grpcclient.Clients, DB *db.Postgres) *CustomerService {
	return &CustomerService{
		Logger: l,
		Clients: client,
		Storage: storage.NewStoragePg(DB),
	}
}