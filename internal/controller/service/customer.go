package service

import (
	"context"
	"fmt"

	cc "gitlab.com/salon5/customer_service/internal/genproto/customer"
	"gitlab.com/salon5/customer_service/pkg/logger"
)

func (c *CustomerService) CreateCustomer(ctx context.Context, req *cc.CreateCustomerReq) (*cc.CreateCustomerResp, error) {
	res, err := c.Storage.Customer().CreateCustomer(req)
	if err != nil {
		fmt.Println(err)
		c.Logger.Error(`Error while inserting customer`, logger.New(`Error with Customer`))
		return &cc.CreateCustomerResp{}, err
	}
	
	return res, nil
}

func (c *CustomerService) GetCustomers(ctx context.Context, req *cc.CustomerID) (*cc.GetCustomerResp, error) {
	res, err := c.Storage.Customer().GetCustomers(req)
	if err != nil {
		c.Logger.Error(`Error while getting CUSTOMER`, logger.New(`Error with Customer`))
		return &cc.GetCustomerResp{}, err
	}
	return res, nil	
}

// func (c *CustomerService) GetCustomerwithSalon(ctx context.Context, req *cc.CustomerID) (*cc.GetCustomerwithSalonResp, error) {
// 	customer, err := c.Storage.Customer().GetCustomers(req)
// 	if err != nil {
// 		c.Logger.Error(`Error while getting CUSTOMER`, logger.New(`Error with Customer`))
// 		return nil, err
// 	}
// return customer, nil
// }