package repo

import (
	cc "gitlab.com/salon5/customer_service/internal/genproto/customer"
	
)
	
type CustomerStorageI interface {
	CreateCustomer(*cc.CreateCustomerReq) (*cc.CreateCustomerResp, error)
	GetCustomers(*cc.CustomerID) (*cc.GetCustomerResp, error)

	CreateAddress(*cc.Address) (*cc.Address, error)
	GetAddress(*cc.CustomerID) ([]*cc.Address, error)
}