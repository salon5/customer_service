package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment      string
	PostgresPort     string
	PostgresHost     string
	PostgresUser     string
	PostgresDatabase string
	PostgresPassword string
	LogLevel         string

	CustomerServicePort string
	CustomerServiceHost string

	SalonServicePort string
	SalonServiceHost string

	AutoInfoServicePort string
	AutoInfoServiceHost string

	PGXPOOLMAX int
}

func Load() *Config {
	c := &Config{}

	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))
	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToString(GetOrReturnDefault("POSTGRES_PORT", "5432"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "citizenfour"))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "customer_evron"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "12321"))
	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))

	c.CustomerServiceHost = cast.ToString(GetOrReturnDefault("CUSTOMER_SERVICE_HOST", "localhost"))
	c.CustomerServicePort = cast.ToString(GetOrReturnDefault("CUSTOMER_SERVICE_PORT", "3333"))

	c.SalonServiceHost = cast.ToString(GetOrReturnDefault("SALON_SERVICE_HOST", "localhost"))
	c.SalonServicePort = cast.ToString(GetOrReturnDefault("SALON_SERVICE_PORT", "4444"))

	c.AutoInfoServiceHost = cast.ToString(GetOrReturnDefault("AUTO_INFO_SERVICE_HOST", "localhost"))
	c.AutoInfoServicePort = cast.ToString(GetOrReturnDefault("AUTO_INFO_SERVICE_PORT", "1111"))

	c.PGXPOOLMAX = cast.ToInt(GetOrReturnDefault("PGX_POOL_MAX", 4))

	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
