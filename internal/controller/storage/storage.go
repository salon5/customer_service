package storage

import (
	"gitlab.com/salon5/customer_service/internal/controller/storage/postgres"
	"gitlab.com/salon5/customer_service/internal/controller/storage/repo"
	"gitlab.com/salon5/customer_service/pkg/db"
)

type IStorage interface {
	Customer() repo.CustomerStorageI
}

type StoragePg struct {
	Db           *db.Postgres
	CustomerRepo repo.CustomerStorageI
}

func NewStoragePg(db *db.Postgres) *StoragePg {
	return &StoragePg{
		Db:           db,
		CustomerRepo: postgres.NewCustomerRepo(db),
	}
}

func (s StoragePg) Customer() repo.CustomerStorageI {
	return s.CustomerRepo
}
