package postgres

import "gitlab.com/salon5/customer_service/pkg/db"

type CustomerRepo struct {
	Db *db.Postgres
}

func NewCustomerRepo(pdb *db.Postgres) *CustomerRepo {
	return &CustomerRepo{Db: pdb}
}
